//
//  FirstViewController.h
//  YoTheGame
//
//  Created by Jonathan on 7/9/14.
//  Copyright (c) 2014 Goforth Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextView *tvChat;

- (IBAction)sendYo:(id)sender;
- (IBAction)sendSup:(id)sender;

@end

