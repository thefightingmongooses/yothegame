//
//  AppDelegate.h
//  YoTheGame
//
//  Created by Jonathan on 7/9/14.
//  Copyright (c) 2014 Goforth Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) MCManager *mcManager;


@end

