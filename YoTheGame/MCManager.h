//
//  MCManager.h
//  YoTheGame
//
//  Created by Jonathan on 7/9/14.
//  Copyright (c) 2014 Goforth Software. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MultipeerConnectivity;

@interface MCManager : NSObject <MCSessionDelegate>

@property (nonatomic, strong) MCPeerID *peerID;
@property (nonatomic, strong) MCSession *session;
@property (nonatomic, strong) MCBrowserViewController *browser;
@property (nonatomic, strong) MCAdvertiserAssistant *advertiser;


-(void)setupPeerAndSessionWithDisplayName:(NSString *)displayName;
-(void)setupMCBrowser;
-(void)advertiseSelf:(BOOL)shouldAdvertise;

@end
