//
//  FirstViewController.m
//  YoTheGame
//
//  Created by Jonathan on 7/9/14.
//  Copyright (c) 2014 Goforth Software. All rights reserved.
//

#import "ChatViewController.h"
#import "AppDelegate.h"

@interface ChatViewController ()

@property (nonatomic, strong) AppDelegate *appDelegate;

-(void)sendMyMessage:(NSString*)message;
-(void)didReceiveDataWithNotification:(NSNotification *)notification;

@end

@implementation ChatViewController
            
- (void)viewDidLoad
{
    [super viewDidLoad];

    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveDataWithNotification:)
                                                 name:@"MCDidReceiveDataNotification"
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendYo:(id)sender
{
    [self sendMyMessage:@"Yo!"];
}

- (IBAction)sendSup:(id)sender
{
    [self sendMyMessage:@"Sup?"];
}

-(void)sendMyMessage:(NSString*)message
{
    NSData *dataToSend = [message dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *allPeers = _appDelegate.mcManager.session.connectedPeers;
    NSError *error;

    if (allPeers.count > 0)
    {
        [_appDelegate.mcManager.session sendData:dataToSend
                                         toPeers:allPeers
                                        withMode:MCSessionSendDataReliable
                                           error:&error];

        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
        }

        [_tvChat setText:[[NSString stringWithFormat:@"ME: %@\n\n", message] stringByAppendingString: _tvChat.text]];
    }
    else
    {
        [_tvChat setText:[@"*** No Connected Peers ***\n\n" stringByAppendingString:_tvChat.text]];
    }


}

-(void)didReceiveDataWithNotification:(NSNotification *)notification
{
    MCPeerID *peerID = [[notification userInfo] objectForKey:@"peerID"];
    NSString *peerDisplayName = peerID.displayName;

    NSData *receivedData = [[notification userInfo] objectForKey:@"data"];
    NSString *receivedText = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];

    [_tvChat performSelectorOnMainThread:@selector(setText:) withObject:[[NSString stringWithFormat:@"%@: %@\n\n", peerDisplayName, receivedText] stringByAppendingString:_tvChat.text] waitUntilDone:NO];
}

@end
