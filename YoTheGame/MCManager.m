//
//  MCManager.m
//  YoTheGame
//
//  Created by Jonathan on 7/9/14.
//  Copyright (c) 2014 Goforth Software. All rights reserved.
//

// TODO List
//
// * switch browse and advertise functions to take the service type
// * switch to using manual connection methods to make connections silent
// * rework system to allow for more than one manager in app
// * implement other connection types


#import "MCManager.h"

static NSString * const SERVICE_NAME = @"yo-game";


@implementation MCManager

-(id)init
{
    self = [super init];

    if (self)
    {
        _peerID = nil;
        _session = nil;
        _browser = nil;
        _advertiser = nil;
    }

    return self;
}

-(void)setupPeerAndSessionWithDisplayName:(NSString *)displayName
{
    _peerID = [[MCPeerID alloc] initWithDisplayName:displayName];

    _session = [[MCSession alloc] initWithPeer:_peerID];
    _session.delegate = self;
}

-(void)setupMCBrowser
{
    _browser = [[MCBrowserViewController alloc] initWithServiceType:SERVICE_NAME session:_session];
}

-(void)advertiseSelf:(BOOL)shouldAdvertise
{
    if (shouldAdvertise)
    {
        _advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:SERVICE_NAME
                                                           discoveryInfo:nil
                                                                 session:_session];
        [_advertiser start];
    }
    else
    {
        [_advertiser stop];
        _advertiser = nil;
    }
}


#pragma mark - MCSessionDelegate Methods


-(void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    NSDictionary *dict = @{@"peerID": peerID,
                           @"state" : [NSNumber numberWithInt:state]
                           };

    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidChangeStateNotification"
                                                        object:nil
                                                      userInfo:dict];
}


-(void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
    NSDictionary *dict = @{@"data": data,
                           @"peerID": peerID
                           };

    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidReceiveDataNotification"
                                                        object:nil
                                                      userInfo:dict];
}


-(void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress
{
    // not used
}


-(void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error
{
    // not used
}


-(void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID{
    // not used
}

@end
