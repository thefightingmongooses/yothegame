Initial build is from Part 1 of the tutorial found here:

http://www.appcoda.com/intro-multipeer-connectivity-framework-ios-programming/


This was created in Xcode 6 Beta 2, but I think it should be compatible with Xcode 5. Let me know if that isn't the case.
